
package com.daioware.time;

public class TimeCounter implements Comparable<TimeCounter>{
	private long beginningTime;
	private long endingTime;
	private long totalTime;
	
	public  boolean equals(Object obj){
		return obj instanceof TimeCounter?((TimeCounter)obj).getNanoseconds()==getNanoseconds():false;
	}
	public int hashCode() {
		return Long.hashCode(getNanoseconds());
	}
	public void start(){
		beginningTime = System.nanoTime();
	}
	public void stop(){
		endingTime = System.nanoTime();
		setTotalTime(endingTime-beginningTime); 
	}
	protected void setTotalTime(long time){
		totalTime=time;
	}
	public long getNanoseconds(){
		return totalTime;
	}
	public long getMilliseconds(){
		return getNanoseconds()/1000000;
	}
	public float getSeconds(){
		return (float)getMilliseconds()/1000;
	}
	public float getMinutes(){
		return getSeconds()/60;
	}
	public float getHours(){
		return getMinutes()/60;
	}
	public float getDays(){
		return getHours()/24;
	}
	public float getWeeks(){
		return getDays()/7;
	}
	@Override
	public int compareTo(TimeCounter o) {
		return Long.compare(getNanoseconds(),o.getNanoseconds());
	}
	
	public String toString() {
		StringBuilder info=new StringBuilder();
		info.append("Weeks:"+getWeeks()).append("\n");
		info.append("Days:"+getDays()).append("\n");
		info.append("getHours:"+getHours()).append("\n");
		info.append("Minutes:"+getMinutes()).append("\n");
		info.append("Seconds:"+getSeconds()).append("\n");
		info.append("Miliseconds:"+getMilliseconds()).append("\n");
		info.append("Nanoseconds:"+getNanoseconds()).append("\n");
		
		return info.toString();
	}
}
