package com.daioware.time;

public class TimeUnit {
	public static final UnitTimeItem NANOS=new Nanos();
	public static final Millis MILLISECONDS=new Millis();
	public static final Seconds SECONDS=new Seconds();
	public static final Minutes MINUTES=new Minutes();
	public static final Hours HOURS=new Hours();
	public static final Days DAYS=new Days();
	public static final Weeks WEEKS=new Weeks();

}
