package com.daioware.time;

public class Nanos implements UnitTimeItem{	
	protected Nanos() {
		
	}
	@Override
	public double toNanos(double v) {
		return v;
	}

	@Override
	public double toMillis(double v) {
		return toNanos(v)/1000000;
	}

	@Override
	public double toSeconds(double v) {
		return toMillis(v)/1000;
	}

	@Override
	public double toMinutes(double v) {
		return toSeconds(v)/60;
	}

	@Override
	public double toHours(double v) {
		return toMinutes(v)/60;
	}

	@Override
	public double toDays(double v) {
		return toHours(v)/60;
	}

	@Override
	public double toWeeks(double v) {
		return toDays(v)/6;
	}

	@Override
	public double toYears(double v) {
		return toDays(v)/365;
	}

	@Override
	public double toDecades(double v) {
		return toYears(v)/10;
	}
	
	@Override
	public void sleep(double value) throws InterruptedException {
		Thread.sleep((long) toMillis(value));
	}
}