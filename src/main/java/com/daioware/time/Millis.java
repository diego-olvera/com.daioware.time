package com.daioware.time;

public class Millis extends Nanos{	
	protected Millis() {
		
	}
	public double toNanos(double v) {
		return toMillis(v)*1000000;
	}

	public double toMillis(double v) {
		return v;
	}
}
