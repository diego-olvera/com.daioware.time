package com.daioware.time;

public class Weeks extends Hours{
	
	protected Weeks() {
		
	}
	public double toDays(double v) {
		return toWeeks(v)/7;
	}
	public double toWeeks(double v) {
		return v;
	}
}
