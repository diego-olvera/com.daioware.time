package com.daioware.time;

public class Seconds extends Millis{	
	protected Seconds() {
		
	}
	public double toMillis(double v) {
		return toSeconds(v)*1000;
	}
	public double toSeconds(double v) {
		return v;
	}
}
