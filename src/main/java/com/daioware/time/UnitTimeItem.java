package com.daioware.time;

public interface UnitTimeItem {

	double toNanos(double v);

	double toMillis(double v);

	double toSeconds(double v);

	double toMinutes(double v);

	double toHours(double v);

	double toDays(double v);

	double toWeeks(double v);

	double toYears(double v);

	double toDecades(double v);
	
	void sleep(double value) throws InterruptedException;


}