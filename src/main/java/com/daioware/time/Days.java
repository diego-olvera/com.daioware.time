package com.daioware.time;

public class Days extends Hours{	
	protected Days() {
		
	}
	public double toHours(double v) {
		return toDays(v)*24;
	}
	public double toDays(double v) {
		return v;
	}
}
