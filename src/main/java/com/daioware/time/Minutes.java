package com.daioware.time;

public class Minutes extends Seconds{
	
	protected Minutes() {
		
	}
	public double toSeconds(double v) {
		return toMinutes(v)*60;
	}
	public double toMinutes(double v) {
		return v;
	}
}
