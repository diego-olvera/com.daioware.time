package com.daioware.time;

public class Hours extends Minutes{	
	protected Hours() {
		
	}
	public double toMinutes(double v) {
		return toHours(v)*60;
	}
	public double toHours(double v) {
		return v;
	}
}
