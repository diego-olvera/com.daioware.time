package com.daioware.time;

public class ProcessTimeCounter {

	private TimeCounter timeCounter;
	private Runnable runnable;
	
	public ProcessTimeCounter(Runnable runnable) {
		setRunnable(runnable);
		setTimeCounter(new TimeCounter());
	}
	
	public TimeCounter getTimeCounter() {
		return timeCounter;
	}

	public Runnable getRunnable() {
		return runnable;
	}

	protected void setTimeCounter(TimeCounter timeCounter) {
		this.timeCounter = timeCounter;
	}

	public void setRunnable(Runnable runnable) {
		this.runnable = runnable;
	}

	public TimeCounter count() {
		timeCounter.start();
		runnable.run();
		timeCounter.stop();
		return timeCounter;
	}
	
	public static TimeCounter count(Runnable runnable) {
		return new ProcessTimeCounter(runnable).count();
	}
	
	
}
