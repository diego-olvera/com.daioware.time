package com.daioWare.time;

import org.junit.Test;

import com.daioware.time.ProcessTimeCounter;

public class ProcessTimeCounterTest {

	@Test
	public void test() {
		Runnable runnable=()->{
			int millis=3*1000;
			System.out.println("Waiting "+millis+" milliseconds...");
			try {
				Thread.sleep(millis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		ProcessTimeCounter counter=new ProcessTimeCounter(runnable);
		System.out.println(counter.count().getMilliseconds());
		System.out.println(ProcessTimeCounter.count(runnable).getMilliseconds());
	}
}
